
#include <stdio.h>
#include <iostream>
#include <cmath>
//#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;

Point center;
Point fre_center;           //Store the central coordinates of the previous frame
int num = 0;
vector<Point> points;

Mat MoveDetect(Mat background, Mat img)
{
    Mat result = img.clone();
    Mat gray1, gray2;
    cvtColor(background, gray1, CV_BGR2GRAY);
    cvtColor(img, gray2, CV_BGR2GRAY);

    Mat diff;
    absdiff(gray1, gray2, diff);
    //imshow("absdiss", diff);

    //cout << "out" << endl;

    threshold(diff, diff, 45, 255, CV_THRESH_BINARY);
    //imshow("threshold", diff);

    Mat element = getStructuringElement(MORPH_RECT, Size(1, 1));
    Mat element2 = getStructuringElement(MORPH_RECT, Size(9, 9));
    erode(diff, diff, element);
    //imshow("erode", diff);
    dilate(diff, diff, element2);
    //imshow("dilate", diff);

    vector<vector<Point> > contours;
    vector<Vec4i> hierarcy;
    findContours(diff, contours, hierarcy, CV_RETR_EXTERNAL, CHAIN_APPROX_NONE);   //Search contour
    vector<Rect> boundRect(contours.size());    //Define a collection of external rectangles
    //drawContours(img2, contours, -1, Scalar(0, 0, 255), 1, 8);  //绘制轮廓
    vector<RotatedRect> box(contours.size());
    int x0 = 0, y0 = 0, w0 = 0, h0 = 0;
    for (int i = 0; i < contours.size(); i++)
    {
        boundRect[i] = boundingRect((Mat) contours[i]);     //Find the outer rectangle for each contour

        x0 = boundRect[i].x;       //Get the 'x' coordinates of the upper left corner of the 'i' outer rectangle
        y0 = boundRect[i].y;       //Get the 'x' coordinates of the upper left corner of the 'i' outer rectangle
        w0 = boundRect[i].width;   //Get the width of the 'i' external rectangle
        h0 = boundRect[i].height;  //Get the height of the 'i' external rectangle

        if (w0 > 200 && h0 > 200)    //Screening for contours greater than 200 in length and width
        {
            num ++;
            rectangle(result, Point(x0, y0), Point(x0+w0, y0+h0), Scalar(0, 255, 0), 2, 8);   //Draw the 'i' external rectangle
            box[i] = fitEllipse(Mat(contours[i]));
            ellipse(result, box[i], Scalar(255, 0, 0), 2, 8);        //Elliptical contour
            circle(result, box[i].center, 3, Scalar(0, 0, 255), -1, 8);    //Painting center
            center = box[i].center;       //Central coordinates of the current frame
            points.push_back(center);     //Centroid points vector set

            if (num != 1)
            {
                //line(result, fre_center, center, Scalar(255, 0, 0), 2, 8);

                //for (int j = 0; j < points.size()-1; j++)
                    //line(result, points[j], points[j+1], Scalar(0, 255, 0), 2, 8);

                stringstream dt;
                if (center.x - fre_center.x > 0)
                {
                    if (center.y - fre_center.y > 0 && abs(center.y-fre_center.y) - abs(center.x-fre_center.x) > 0)
                        dt << "direct: down" << "       ";
                    else if (abs(center.y-fre_center.y) - abs(center.x-fre_center.x) < 0)
                        dt << "direct: right" << "       ";
                    else {
                        dt << "direct: up" << "       ";
                    }
                }
                else
                {
                    if (center.y - fre_center.y > 0 && abs(center.y-fre_center.y) - abs(center.x-fre_center.x) > 0)
                        dt << "direct: down" << "       ";
                    else if (abs(center.y-fre_center.y) - abs(center.x-fre_center.x) < 0)
                        dt << "direct: left" << "       ";
                    else {
                        dt << "direct: up" << "       ";
                    }
                }

                putText(result, dt.str(), cv::Point(10, 25), CV_FONT_HERSHEY_COMPLEX, 1, cv::Scalar(0, 255, 255));
            }

            fre_center = center;
        }
    }

    return result;
}

int main()
{

    VideoCapture capture(0);
    //VideoCapture capture("/Users/aiden_zj/Aiden/iVideo/blinks_one.mov");

    if (!capture.isOpened())
    {
        cout << "Unable to open camera device, check it is properly connected!" << endl;
        return -1;
    }

    Mat frame, background, result;
    int count = 0;
    while (1)
    {
        capture >> frame;

        if (!frame.empty())
        {
            count++;
            if (count == 1)
                background = frame.clone();          //Extract the first frame as the background frame
                //imshow("video", frame);
                //cout << "show" << endl;
                result = MoveDetect(background, frame);
                imshow("result", result);
                if (waitKey(30) == 27)
                {
                    break;
                }
        }
        else
        {
            cout << "frame empty!" << endl;
            break;
        }
    }

    capture.release();

    return 0;
}


